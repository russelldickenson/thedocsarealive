---
title: First post!
date: 2015-01-05
---

Hi everyone. I think you'd agree it's been an eventful century so far for mankind! The challenges
faced because of the COVID-19 global epidemic, and advances in space exploration.
